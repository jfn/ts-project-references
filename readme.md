# Typescript project references

Typescript 3.0 introduced project references, so you can do incremental
builds. This repo is a proof of concept of how that could work for a project
with a `types` package, and an `API` package that has a dependency to
`types`.

## How to run

```
git clone <url>
cd ts-project-references

npm run api
```

When you run this:
* We add a symlink from `packages/api/node_modules/types` to `packages/api`
* Build `package/api`. Since its `tsconfig.json` depends on `packages/types`,
Typescript will first build `packages/types`.

## Useful links

* [https://www.typescriptlang.org/docs/handbook/project-references.html](official docs)
* [https://github.com/Microsoft/TypeScript](typescript official repo uses this strategy)

## What I don't like in this strategy

It seems that right now, vscode isn't smart enough to run `tsc --build` on
the background, so right after you checkout your code, you'll have errors,
until you build for the first time.

After you build, vscode will use whatever definitions you have in
`packages/types/dist`. This means that intelliSense will always be using the
definitions from your last build for providing feedback. Even though it will
allow you to navigate to the source and see the definition you want, it
will be showing code-completion and errors based on "outdated" information
until you build again.

To avoid this, you can be running Typescript in watch mode, so vscode can
pick up the changes, and present you the right feedback

```
# From project root
npm run build:watch
```

## Other notes

In this demo I have a dependency to `typescript@next`, because I wanted to
validate that I wasn't running into any bug that already been addressed with
Typescript. But you only need 3.0 to use these features.
